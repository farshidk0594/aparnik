import React from 'react'
import ReactDOM from 'react-dom'
import './assets/css/PersianFont.css'
import './assets/css/EnglishFont.css'
import App from './App'

const rootEl = document.getElementById('main')
ReactDOM.render(<App />, rootEl)
