export default {
  primary: {
    main: '#2c237c',
    white: '#FFF',
    googleText: '#69b3b7'
  },

  secondary: {
    main: '#e7b700'
  }
}
