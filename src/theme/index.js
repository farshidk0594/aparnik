import { createMuiTheme } from '@material-ui/core/styles'
import { getFont, getDirection } from '../localization/languages'
import palette from './palette'

const Theme = createMuiTheme({
  direction: getDirection(),
  palette,
  typography: {
    useNextVariants: true,
    fontFamily: getFont(),
    fontWeightRegular: 600,
    fontWeightMedium: 800,
    fontSize: 16,
    fontStyle: 'normal',
    fontStretch: 'normal',
    letterSpacing: 'normal',
    fontWeight: 600,
    lineHeight: 1,
    color: palette.primary.main
  }
})

export default Theme
