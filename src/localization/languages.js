import Moment from 'moment-jalaali'

const directions = {
  fa: 'rtl',
  en: 'ltr'
}
const fonts = {
  fa: 'IRANSans',
  en: 'Graphico'
}
const locales = {
  fa: 'fa-ir',
  en: 'en-us'
}
const lang = localStorage.lang ? localStorage.lang : 'en'
export function getLanguage() {
  return lang
}
export function getDirection() {
  return directions[lang]
}
export function getFont() {
  return fonts[lang]
}
export function setMomentLocale() {
  Moment.locale(locales[lang])
  if (lang === 'fa') {
    Moment.loadPersian({ dialect: 'persian-modern' })
  }
}
