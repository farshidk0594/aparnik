import React, { useState } from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import leftBackgroundImage from '../../assets/images/icon4Asset6mdpi.png'
import cloudsImage from '../../assets/images/icon4Asset3mdpi.png'
import helicopterImage from '../../assets/images/icon4Asset1mdpi.png'
import logoImage from '../../assets/images/icon4Asset17mdpi.png'
import MenuIcon from '../../assets/images/icon4Asset8mdpi.png'
import useStyles from './indexStyles'
import './css/effect.css'
import SingnInForm from '../../components/SingnInForm'
import SignUpForm from '../../components/SignUpForm'

export default function index() {
  const classes = useStyles()
  const [page, setPage] = useState(0)

  return (
    <Grid container className={classes.rootGrid}>
      <Grid item lg={6} className={classes.leftGrid}>
        <img src={logoImage} alt="Aparnik" className={classes.leftLogoImg} />
        <Typography variant="h3" className={classes.leftTitle}>
          Great Marketplace for Drones World
        </Typography>
        <div className="stars" />
        <div id="stars2" />
        <div id="stars3" />
        <img
          src={leftBackgroundImage}
          alt="Aparnik"
          className={classes.leftBgImg}
        />
        <img
          src={cloudsImage}
          alt="Aparnik"
          className={classes.leftCloudsImg}
        />
        <img
          src={helicopterImage}
          alt="Aparnik"
          className={classes.leftHelicopterImg}
        />
      </Grid>
      <Grid item lg={6} className={classes.rightGrid}>
        <Grid container direction="column" style={{ padding: 80 }}>
          <Grid container direction="row" className={classes.titleGridForm}>
            <Grid item>
              <Button
                className={classes.pageButton}
                color={page === 0 ? 'default' : 'secondary'}
                onClick={() => setPage(0)}
                style={
                  page === 0
                    ? {
                        borderBottomWidth: 1,
                        borderBottomColor: '#000',
                        borderBottomStyle: 'solid'
                      }
                    : {}
                }
              >
                Sign Up
              </Button>
              <span>or</span>
              <Button
                className={classes.pageButton}
                color={page === 1 ? 'default' : 'secondary'}
                onClick={() => setPage(1)}
                style={
                  page === 1
                    ? {
                        borderBottomWidth: 1,
                        borderBottomColor: '#000',
                        borderBottomStyle: 'solid'
                      }
                    : {}
                }
              >
                Sign In
              </Button>
            </Grid>
            <Grid item>
              <IconButton aria-label="delete">
                <img src={MenuIcon} alt="Aparnik" width={40} />
              </IconButton>
            </Grid>
          </Grid>
          {page === 0 ? <SignUpForm /> : <SingnInForm />}
        </Grid>
      </Grid>
    </Grid>
  )
}
