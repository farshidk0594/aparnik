import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  rootGrid: {
    minHeight: '100vh'
  },
  rightGrid: {
    flex: 1,
    backgroundColor: theme.palette.primary.white
  },
  leftGrid: {
    flex: 1,
    backgroundColor: theme.palette.primary.main,
    boxShadow: 'rgba(0, 0, 0, 0.64) 0px 0px 5px 3px',
    zIndex: 1,
    position: 'relative'
  },
  leftLogoImg: {
    position: 'absolute',
    top: 50,
    width: 240,
    left: 30
  },
  leftTitle: {
    color: theme.palette.primary.white,
    fontSize: 28,
    position: 'absolute',
    top: 200,
    left: 40,
    fontWeight: 700
  },
  leftBgImg: {
    width: '100%',
    height: '70%',
    position: 'absolute',
    bottom: 0
  },
  leftCloudsImg: {
    width: '100%',
    position: 'absolute',
    bottom: '35%',
    left: '-10%',
    zIndex: -1
  },
  leftHelicopterImg: {
    width: '35%',
    position: 'absolute',
    bottom: '25%',
    left: '55%',
    zIndex: -1
  },
  pageButton: {
    textTransform: 'none',
    fontWeight: 500,
    fontSize: 20
  },
  titleGridForm: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 40
  }
}))
export default useStyles
