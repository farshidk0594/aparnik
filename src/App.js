import React from 'react'
import { Provider } from 'react-redux'
import { MuiThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import { IntlProvider } from 'react-intl'

// eslint-disable-next-line import/no-extraneous-dependencies
import { create } from 'jss'
import rtl from 'jss-rtl'
// eslint-disable-next-line import/no-extraneous-dependencies
import { StylesProvider, jssPreset } from '@material-ui/styles'
import Theme from './theme'
import configureStore from './store'
import Routes from './Routes'
import {
  getDirection,
  getLanguage,
  setMomentLocale
} from './localization/languages'
import translations from './localization/index'

const jssRtl = create({ plugins: [...jssPreset().plugins, rtl()] })
const jssLtr = create({ plugins: [...jssPreset().plugins, null] })

setMomentLocale()

const store = configureStore()

export default function App() {
  const jss = getDirection() === 'rtl' ? jssRtl : jssLtr
  document.body.setAttribute('dir', getDirection())
  return (
    <Provider store={store}>
      <MuiThemeProvider theme={Theme}>
        <>
          <CssBaseline />
          <StylesProvider jss={jss}>
            <IntlProvider
              locale={getLanguage()}
              messages={translations[getLanguage()]}
            >
              <Routes />
            </IntlProvider>
          </StylesProvider>
        </>
      </MuiThemeProvider>
    </Provider>
  )
}
