import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    paddingRight: 20,
    paddingLeft: 20,
    width: '65%'
  },
  textField: {
    marginTop: 20
  },
  input: {
    '&::placeholder': {
      fontSize: 14
    }
  },
  facebookButton: {
    textTransform: 'none',
    fontSize: 14,
    fontWeight: 500,
    marginTop: 10,
    '& >span': {
      textAlign: 'left',
      justifyContent: 'unset'
    }
  },
  forgetPasswordButton: {
    textTransform: 'none',
    fontSize: 14,
    fontWeight: 500,
    marginTop: 10,
    '& >span': {
      textAlign: 'left',
      justifyContent: 'unset'
    }
  },
  googleButton: {
    textTransform: 'none',
    fontSize: 14,
    textAlign: 'left',
    fontWeight: 500,
    color: theme.palette.primary.googleText,
    marginBottom: 15,
    '& >span': {
      textAlign: 'left',
      justifyContent: 'unset'
    }
  },
  signinButton: {
    textTransform: 'none',
    fontSize: 14,
    width: 120,
    borderRadius: 14
  }
}))
export default useStyles
