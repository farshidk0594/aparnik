import React from 'react'
import Grid from '@material-ui/core/Grid'
// import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import useStyles from './indexStyles'

export default function index() {
  const classes = useStyles()
  return (
    <Grid container direction="column" className={classes.root}>
      <TextField
        required
        label="Email"
        placeholder="Your Email Goes Here"
        InputLabelProps={{
          shrink: true
        }}
        InputProps={{
          classes: { input: classes.input }
        }}
        className={classes.textField}
      />
      <TextField
        required
        type="password"
        label="Password"
        placeholder="******"
        InputLabelProps={{
          shrink: true
        }}
        InputProps={{
          classes: { input: classes.input }
        }}
        className={classes.textField}
      />

      <Button className={classes.facebookButton}>-Sign in with Facebook</Button>
      <Button className={classes.googleButton}>-Sign in with Google</Button>
      <Button
        variant="contained"
        color="secondary"
        className={classes.signinButton}
      >
        Sign In
      </Button>
      <Button className={classes.forgetPasswordButton}>Forget Password?</Button>
    </Grid>
  )
}
