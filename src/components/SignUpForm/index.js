import React, { useState } from 'react'
import Grid from '@material-ui/core/Grid'
// import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import Typography from '@material-ui/core/Typography'
import useStyles from './indexStyles'

export default function index() {
  const classes = useStyles()
  const [termcheck, setTermcheck] = useState(false)
  return (
    <Grid container direction="column" className={classes.root}>
      <TextField
        required
        label="Full Name"
        placeholder="Enter Your Name"
        InputLabelProps={{
          shrink: true
        }}
        InputProps={{
          classes: { input: classes.input }
        }}
        className={classes.textField}
      />
      <TextField
        required
        label="Email"
        placeholder="Your Email Goes Here"
        InputLabelProps={{
          shrink: true
        }}
        InputProps={{
          classes: { input: classes.input }
        }}
        className={classes.textField}
      />
      <TextField
        type="password"
        required
        label="Password"
        placeholder="******"
        InputLabelProps={{
          shrink: true
        }}
        InputProps={{
          classes: { input: classes.input }
        }}
        className={classes.textField}
      />
      <TextField
        required
        type="password"
        label="Confirm Password"
        placeholder="******"
        InputLabelProps={{
          shrink: true
        }}
        InputProps={{
          classes: { input: classes.input }
        }}
        className={classes.textField}
      />
      <Grid container direction="row" className={classes.termsFormGrid}>
        <Checkbox
          checked={termcheck}
          onChange={() => setTermcheck((termcheck) => !termcheck)}
        />
        <Typography
          onClick={() => {
            setTermcheck((termcheck) => !termcheck)
          }}
          className={classes.termText}
        >
          i`ve got all statemanet in &nbsp;
        </Typography>
        <a href="http://" alt="" className={classes.termButton}>
          terms and conditions
        </a>
      </Grid>
      <Grid container direction="row" className={classes.footerFormGrid}>
        <Button
          variant="contained"
          color="secondary"
          className={classes.signupButton}
        >
          Let`s Go
        </Button>
        <Button className={classes.signinButton}>
          are you already a member?
        </Button>
      </Grid>
    </Grid>
  )
}
