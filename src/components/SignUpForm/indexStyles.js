import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() => ({
  root: {
    paddingRight: 20,
    paddingLeft: 20,
    width: '65%'
  },
  textField: {
    marginTop: 20
  },
  input: {
    '&::placeholder': {
      fontSize: 14
    }
  },
  signinButton: {
    textTransform: 'none',
    fontSize: 14,
    fontWeight: 500,
    marginTop: 10,
    '& >span': {
      textAlign: 'left',
      justifyContent: 'unset'
    }
  },
  termButton: {
    textTransform: 'none',
    fontSize: 12,
    fontWeight: 500,
    '& >span': {
      textAlign: 'left',
      justifyContent: 'unset'
    }
  },
  signupButton: {
    textTransform: 'none',
    fontSize: 14,
    width: 100,
    borderRadius: 14
  },
  footerFormGrid: {
    marginTop: 25
  },
  termsFormGrid: {
    marginTop: 40,
    display: 'flex',
    alignItems: 'center'
  },
  termText: {
    fontWeight: 500,
    fontSize: 12
  }
}))
export default useStyles
