import { combineReducers } from 'redux'
import ReduxForm from './ReduxForm'

const reducers = combineReducers({
  form: ReduxForm
})

export default reducers
