const HtmlWebPackPlugin = require('html-webpack-plugin')
const path = require('path')

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
        ]
      },
      {
        test: /\.(ttf|eot|woff|woff2|otf)$/,
        use: {
          loader: 'file-loader'
        }
      },
      {
        test: /\.css$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }]
      },
      {
        test: /\.(png|jpg|svg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              limit: 8000, // Convert images < 8kb to base64 strings
              // name: 'static/[name].[ext]'
              name() {
                if (process.env.NODE_ENV === 'development') {
                  return '[path][name].[ext]'
                }
                return 'static/[hash].[ext]'
              }
            }
          }
        ]
      }
    ]
  },
  output: {
    path: path.resolve('./dist'),
    filename:
      process.env.NODE_ENV === 'production'
        ? '[name].[contenthash].js'
        : '[name].[hash].js',
    publicPath: '/'
  },
  plugins: [
    new HtmlWebPackPlugin({
      // favicon: './src/assets/images/favicon.ico',
      template: './src/index.html',
      filename: './index.html'
    })
  ],
  devServer: {
    historyApiFallback: true,
    compress: true,
    disableHostCheck: true
    // https:true,
  }
}
